package com.jimson.pierrelouis;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ComputeActivity extends AppCompatActivity {

    private TextView textViewNb1, textViewNb2;
    private Button button_Sum,button_Minus,button_Multiply,button_Divide;

    public static final  int DIVIDE_BY_ZERO=-1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compute);


        textViewNb1=findViewById(R.id.textNb1);
        textViewNb2=findViewById(R.id.textNb2);
        button_Sum=findViewById(R.id.sum);
        button_Minus=findViewById(R.id.minus);
        button_Multiply=findViewById(R.id.multiply);
        button_Divide=findViewById(R.id.divide);
        setUpViews();
        initViews();
    }

    private void setUpViews(){
        /**un objet intent pour appeler l'autre vue*/

        Intent intent=getIntent();
        /**les valeurs des cles dans l'intent*/
        String nb1=intent.getStringExtra("nb1");
        String nb2=intent.getStringExtra("nb2");

        textViewNb1.setText(nb1);;
        textViewNb2.setText(nb2);

    }

    private void initViews(){
        String nb1=getIntent().getStringExtra("nb1");
        String nb2=getIntent().getStringExtra("nb2");

        Intent intent=new Intent();

        button_Sum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Integer result=Integer.parseInt(String.valueOf(nb1))+Integer.parseInt(String.valueOf(nb2));
               intent.putExtra("result", result.toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        button_Minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer result=Integer.parseInt(String.valueOf(nb1)) - Integer.parseInt(String.valueOf(nb2));
                intent.putExtra("result", result.toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        button_Multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer result=Integer.parseInt(String.valueOf(nb1)) * Integer.parseInt(String.valueOf(nb2));
                intent.putExtra("result", result.toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        button_Divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Integer.parseInt(String.valueOf(nb2))==0){
                    Toast.makeText(ComputeActivity.this,"wrong divider",Toast.LENGTH_SHORT).show();
                }
                else{
                   Float result=Float.parseFloat(String.valueOf(nb1)) / Float.parseFloat(String.valueOf(nb2));
                    intent.putExtra("result", result.toString());
                    setResult(RESULT_OK, intent);
                }
               finish();
            }
        });


    }


}
