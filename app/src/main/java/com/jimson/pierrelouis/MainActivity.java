package com.jimson.pierrelouis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity
{

    private EditText editTextNb1;
    private EditText editTextNb2;
    private Button buttonCompute;
    private int COMPUTE_CODE = 123;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextNb1 = findViewById(R.id.nb1);
        editTextNb2 = findViewById(R.id.nb2);
        buttonCompute = findViewById(R.id.compute);
        initViews();
    }

    private void initViews()
    {
        Context context = getBaseContext();

        Intent intent = new Intent(context, ComputeActivity.class);

        editTextNb1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextNb1.getText().length() > 0 && editTextNb2.getText().length() > 0) {
                    buttonCompute.setEnabled(true);
                } else {
                    buttonCompute.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });

        editTextNb2.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (editTextNb1.getText().length() > 0 && editTextNb2.getText().length() > 0) {
                    buttonCompute.setEnabled(true);
                } else {
                    buttonCompute.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        /**intercepter click sur le bouton*/
        buttonCompute.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String textNb1 = editTextNb1.getText().toString();
                String textNb2 = editTextNb2.getText().toString();

                intent.putExtra("nb1", textNb1);
                intent.putExtra("nb2", textNb2);
                startActivityForResult(intent, COMPUTE_CODE);
                Toast.makeText(MainActivity.this, "on va agir sur " + textNb1 + " et " + textNb2, Toast.LENGTH_SHORT).show();
            }

        });
    }

        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (resultCode == RESULT_OK && requestCode == COMPUTE_CODE && data != null) {
            String result = data.getStringExtra("result");
            Toast.makeText(this, "result is: " + result, Toast.LENGTH_SHORT).show();
            }


    }

}